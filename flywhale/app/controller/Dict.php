<?php

namespace app\controller;

use app\service\DictService;
use think\annotation\Inject;

class Dict extends Base
{
    /**
     * @Inject()
     * @var DictService
     */
    protected $dictService;

    public function index()
    {
        $param = input('param.');

        $res = $this->dictService->getDictList($param);

        return json(pageReturn($res));
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->dictService->addDict($param);
            return json($res);
        }
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $res = $this->dictService->editDict($param);
            return json($res);
        }
    }

    public function del()
    {
        $id = input('param.id');

        $res = $this->dictService->delDict($id);
        return json($res);
    }

    public function getDictByCateId()
    {
        $cateId = input('param.cate_id');

        $res = $this->dictService->getInfoByCateId($cateId);
        return json($res);
    }
}