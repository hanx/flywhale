<?php

namespace app\model;

use think\facade\Log;

class Role extends BaseModel
{
    /**
     * 获取角色信息
     * @param $id
     * @return array
     */
    public function getRoleById($id)
    {
        try {

            $info = $this->where('id', $id)->find();
        } catch (\Exception $e) {
            Log::error('根据ID获取角色错误: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $info);
    }

    /**
     * 获取角色列表
     * @param $limit
     * @param $where
     * @return array
     */
    public function getRoleList($limit, $where)
    {
        try {

            $list = $this->where($where)->order('id', 'desc')->paginate($limit);
        }catch (\Exception $e) {
            Log::error('获取角色列表错误: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 添加角色
     * @param $param
     * @return array
     */
    public function addRole($param)
    {
        try {

            $info = $this->where('name', $param['name'])->find();
            if (!empty($info)) {
                return dataReturn(-2, '该角色已经存在');
            }

            $param['create_time'] = date('Y-m-d H:i:s');
            $this->insert($param);
        }catch (\Exception $e) {
            Log::error('添加角色失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '添加成功');
    }

    /**
     * 编辑角色
     * @param $param
     * @return array
     */
    public function editRole($param)
    {
        try {

            if ($param['id'] == 1) {
                return dataReturn(-3, '超级管理员不可编辑');
            }

            $info = $this->where('name', $param['name'])->where('id', '<>', $param['id'])->find();
            if (!empty($info)) {
                return dataReturn(-2, '该角色已经存在');
            }

            $param['update_time'] = date('Y-m-d H:i:s');
            $this->where('id', $param['id'])->update($param);
        }catch (\Exception $e) {
            Log::error('编辑角色失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '编辑成功');
    }

    /**
     * 删除角色
     * @param $roleId
     * @return array
     */
    public function delRole($roleId)
    {
        try {

            if ($roleId == 1) {
                return dataReturn(-3, '超级管理员不可删除');
            }

            $this->where('id', $roleId)->delete();
        }catch (\Exception $e) {
            Log::error('删除角色失败: ' . $e->getMessage() . PHP_EOL  . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, '删除成功');
    }
}