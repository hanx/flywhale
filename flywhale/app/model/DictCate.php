<?php

namespace app\model;

use think\facade\Log;

class DictCate extends BaseModel
{
    /**
     * 获取字典分类列表
     * @return array
     */
    public function getDictCateList()
    {
        try {

            $list = $this->select();
        } catch (\Exception $e) {
            Log::error('获取字典分类错误: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success', $list);
    }

    /**
     * 添加字典分类
     * @param $param
     * @return array
     */
    public function addDictCate($param)
    {
        try {

            $has = $this->where('code', $param['code'])->find();
            if (!empty($has)) {
                return dataReturn(-2, '该字典分类已经存在');
            }

            $this->insert($param);
        } catch (\Exception $e) {
            Log::error('添加字典分类错误: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success');
    }

    /**
     * 编辑字典分类
     * @param $param
     * @return array
     */
    public function editDictCate($param)
    {
        try {

            $has = $this->where('code', $param['code'])->where('id', '<>', $param['id'])->find();
            if (!empty($has)) {
                return dataReturn(-2, '该字典分类已经存在');
            }

            $this->where('id', $param['id'])->update($param);
        } catch (\Exception $e) {
            Log::error('编辑字典分类错误: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success');
    }

    /**
     * 删除字典分类
     * @param $id
     * @return array
     */
    public function delDictCate($id)
    {
        try {

            $has = $this->where('pid', $id)->find();
            if (!empty($has)) {
                return dataReturn(-2, '该分类下有子分类，不可删除');
            }

            $this->where('id', $id)->delete();
        } catch (\Exception $e) {
            Log::error('删除字典分类错误: ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, 'success');
    }
}