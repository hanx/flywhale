import http from "@/utils/request"

export default {
	list: {
		url: '/dept/index',
		name: "获取部门列表",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	},
	add: {
		url: '/dept/add',
		name: "添加部门",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	},
	edit: {
		url: '/dept/edit',
		name: "编辑部门",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	},
	del: {
		url: '/dept/del',
		name: "删除部门",
		get: async function(data={}) {
			return await http.get(this.url, data);
		}
	}
}