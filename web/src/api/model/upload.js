import http from "@/utils/request"

export default {
	doupload: {
		url: '/upload/img',
		name: "上传文件",
		post: async function(data={}) {
			return await http.post(this.url, data);
		}
	}
}