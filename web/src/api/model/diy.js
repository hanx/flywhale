import http from "@/utils/request"

export default {
    list: {
        url: '/diy/index',
        name: "自定义列表的数据",
        get: async function (data = {}) {
            return await http.get(this.url, data);
        }
    },
    form: {
        url: '/diy/getForm',
        name: "新增自定义数据",
        get: async function (data = {}) {
            return await http.get(this.url, data);
        }
    },
    add: {
        url: '/diy/add',
        name: "新增自定义数据",
        post: async function (data = {}) {
            return await http.post(this.url, data);
        }
    },
    info: {
        url: '/diy/getInfo',
        name: "获取自定义数据",
        get: async function (data = {}) {
            return await http.get(this.url, data);
        }
    },
    edit: {
        url: '/diy/edit',
        name: "编辑自定义数据",
        post: async function (data = {}) {
            return await http.post(this.url, data);
        }
    },
    del: {
        url: '/diy/del',
        name: "删除自定义数据",
        get: async function (data = {}) {
            return await http.get(this.url, data);
        }
    }
}